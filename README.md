This little project is meant to find out how much the general public can 
reverse engineer of the recent Facebook birthday hack. 

It leverages transparency tools that have been mandated by the new European
Data Protection Regulation. Unfortunately Facebook has only partially 
implemented its obligations, but let's see how far we can get!

The folder `donated_data/` contains donated personal data that might be relevant.

The idea is to build open source software to analyse this data, so it can be 
pushed to anyone who wishes in order to do a local analysis. 

You can join us and discuss this on [our Slack channel](http://bit.ly/join-pdio).

You can start the computing environment here:
[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/PersonalDataIO%2Ffacebook-birthday-breach/master)
